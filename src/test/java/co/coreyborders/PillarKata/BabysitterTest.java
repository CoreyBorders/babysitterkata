package co.coreyborders.PillarKata;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BabysitterTest {
	private Babysitter babysitter;
	
	@Before
	public void setup() {
		babysitter = new Babysitter();
	}
	
	@Test
	public void whenStartTimeIsLessThan5pmCalculateReturnsTooEarlyMessage() {
		Assert.assertEquals("Cannot Start Until 5pm", babysitter.calculateNightlyRate(13, 17, 18));
	}
	
	@Test
	public void StartInTheMorningShowsTooEarlyMessage() {
		try {
			babysitter.calculateNightlyRate(4, 13, 17);
			Assert.fail();
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void whenStartTimeIsAfter5pmCalculateReturnsValueOfStartTimeIn24HourClock() {
		Assert.assertEquals("Start time: 17", babysitter.calculateNightlyRate(17, 19, 20));
	}
	
	@Test
	public void startTimesAfterMidnightShouldReturn24HoursAfterActualTimeToIndicateThatTheyAreTheNextDay() {
		Assert.assertEquals(32, babysitter.calculateNightlyRate(1, 2, 3));
	}
	
	@Test
	public void endTimesAfter4amReturnErrorMessage() {
		try {
			babysitter.calculateNightlyRate(20, 24, 5);
			Assert.fail();
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void endTimesBefore4amReturnValueOfEndTime() {
		Assert.assertEquals(28, babysitter.calculateNightlyRate(20, 24, 4));
	}
	
	@Test
	public void shiftFrom5To7WillReturnOnePreBedTimeHours() {
		Assert.assertEquals(1, babysitter.calculateNightlyRate(17, 19, 19));
	}
	
	@Test
	public void postBedTimeHoursWillFindHoursBetweenBedTimeAndMidnight() {
		Assert.assertEquals(2, babysitter.calculateNightlyRate(22, 21, 24));
	}
	
	@Test
	public void afterMidnightHoursWillFindHoursBetweenMidnightAndEndTime() {
		Assert.assertEquals(3, babysitter.calculateNightlyRate(24, 21, 3));
	}
	
	@Test
	public void onePreBedTimeHourReturns12DollarTotalPayment() {
		Assert.assertEquals(12, babysitter.calculateNightlyRate(17, 18, 18));
	}
	
	@Test
	public void onePostBedTimeHourReturns8DollarTotalPayment() {
		Assert.assertEquals(8, babysitter.calculateNightlyRate(19, 18, 20));
	}
	
	@Test
	public void onePostMidnightHourReturns16DollarTotalPayment() {
		Assert.assertEquals(16, babysitter.calculateNightlyRate(24, 20, 1));
	}
	
	@Test
	public void startTime8pmBedTime9pmEndTime10pmShouldReturn20Dollars() {
		Assert.assertEquals(20, babysitter.calculateNightlyRate(20, 21, 22));
	}
	
	@Test
	public void startTime8pmBedTime9pmEndTime1amShouldReturn52Dollars() {
		Assert.assertEquals(52, babysitter.calculateNightlyRate(20, 21, 1));
	}
	
	@Test
	public void startTimeMidnightBedTime8pmEndTime1amShouldReturn16Dollars() {
		Assert.assertEquals(16, babysitter.calculateNightlyRate(24, 21, 1));
	}

}
