package co.coreyborders.PillarKata;

public class Babysitter {
	
	private int preBedTimeHours;
	private int postBedTimeHours;
	private int postMidnightHours;
	private int handledEndTime;
	private int handledStartTime;
	private int totalPayment;
	

	public int calculateNightlyRate(int startTime, int bedTime, int endTime) throws IllegalArgumentException{
		findPreBedTimeHours(startTime, bedTime);		
		findPostBedTimeHours(bedTime, endTime);
		findPostMidnightHours(endTime);
		findTotalPayment(preBedTimeHours, postBedTimeHours, postMidnightHours);
		
		System.out.println("Start time: " + startTime);
		System.out.println("Bed time: " + bedTime);
		System.out.println("End time: " + endTime);
		System.out.println("Total payment due: $" + totalPayment);
		
		return totalPayment;
	}
	
	private int handleStartTime(int startTime) throws IllegalArgumentException {
		if(startTime >= 1 && startTime < 4) {
			handledStartTime = makeNextDay(startTime);
		} else if(startTime < 17) {
			throw new IllegalArgumentException("Cannot Start Until 5pm");
		} else {
			handledStartTime = startTime;
		}
		return handledStartTime;
	}
	
	private int handleEndTime(int endTime) throws IllegalArgumentException{
		if(endTime > 28) {
			throw new IllegalArgumentException("Must End By 4am");
		} else if(endTime >= 1 && endTime <= 12) {
			handledEndTime = makeNextDay(endTime);
		} else {
			handledEndTime = endTime;
		}
		return handledEndTime;
	}
	
	private int findPreBedTimeHours(int startTime, int bedTime) throws IllegalArgumentException{
		handleStartTime(startTime);
		
		if(handledStartTime < bedTime) {
			preBedTimeHours = (bedTime - startTime);
		} else {
			preBedTimeHours = 0;
		}
		
		return preBedTimeHours;
	}
	
	private int findPostBedTimeHours(int bedTime, int endTime) throws IllegalArgumentException{
		handleEndTime(endTime);
		
		if(bedTime < 24 && handledStartTime < 24) {
			if(handledEndTime < 24) {
				postBedTimeHours = (handledEndTime - bedTime);
			} else {
				postBedTimeHours = (24 - bedTime);
			}
		} else {
			postBedTimeHours = 0;
		}
		return postBedTimeHours;
	}
	
	private int findPostMidnightHours(int endTime) throws IllegalArgumentException{
		handleEndTime(endTime);
		
		if(handledEndTime > 24) {
			postMidnightHours = (handledEndTime - 24);
		} else {
			postMidnightHours = 0;
		}
		return postMidnightHours;
	}
	
	private int findTotalPayment(int preBedTimeHours, int postBedTimeHours, int postMidnightHours) {
		totalPayment = (preBedTimeHours * 12) + (postBedTimeHours * 8) + (postMidnightHours * 16);
		return totalPayment;
	}
	
	private int makeNextDay(int time) {
		time += 24;
		return time;
	}
	
}



