THOUGHT PROCESS TO MY APPROACH:

My thought process when starting this kata was to break the problem down into small enough steps that I could write tests with as few assertions as possible, ideally only one, and have that one test cover the logic I needed it to, then go back and add in more tests to treat edge cases if necessary.

My first goal was to get the application to not allow hours outside the specified timeframe of 5pm to 4 am.  I tested the method that wouldn't allow a start time with a value less than 5, but when I began to write the test to check for end times after 4am I realized I would have to switch over to a 24 hour clock to make things a little easier.

Initially I began to write tests that would return the correct dollar values for the hours worked, but I realized that that was somewhat cumbersome and that I was actually doing two different things in the same method.  There's no reason the method that calculates number of hours needs to know about the nightly rate, and no reason the method to calculate payment needs to know how to figure out how many hours passed between start time and bed time.  Once I broke those out things began to go a little more smoothly and I felt better about the discrete pieces of logic I had separated out.

I did run into a little bit of trouble with times in the early morning hours, the math wasn't coming out right since 0400 (4am) and 1700 (5pm) are actually the same day, so I ended up adding 24 hours to the early morning hours so that it was clear mathmatically that the times reflected the next day.


RUNNING THE APPLICATION:

Assuming you have Maven installed and configured, and you are in the project directory, you can run all the tests I have written with the command:

`mvn test`

To run the application use the following commands:

`mvn compile`
`java -cp target/PillarKata-0.0.1-SNAPSHOT.jar co.coreyborders.PillarKata.Main`